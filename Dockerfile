FROM python:3.11-slim

ARG USERNAME=user

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

WORKDIR /prj

RUN adduser $USERNAME

RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        graphviz \
        imagemagick \
        make \
        git \
    && apt-get autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ARG VERSION
ENV VERSION=${VERSION:-6.2.1}

COPY requirements.txt /prj
RUN pip install --no-cache-dir -U pip \
    && pip install --no-cache-dir Sphinx==${VERSION} \
    && pip install --no-cache-dir -r /prj/requirements.txt

RUN chown -R $USERNAME:$USERNAME /prj
USER $USERNAME

CMD ["make", "html"]
