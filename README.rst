docker-sphinx
*************

Docker image ``sanrep/sphinx`` to use Sphinx for creating documentation.

Besides Sphinx, image includes:

- sphinx-intl
- sphinx_rtd_theme
- sphinx_book_theme
- weasyprint
- git+https://github.com/useblocks/sphinx-simplepdf.git (the most recent version)
- doc8

Image is tagged with Sphinx version.

Pull the image::

    $ docker pull sanrep/sphinx:6.2.1

Creating documentation
======================
Run docker container from the sphinx documentation directory::

    $ cd /path/docs
    $ docker run --rm -u $(id -u):$(id -g) -v .:/prj sanrep/sphinx make html

Building image
==============
Create ``sanrep/sphinx`` image::

    $ docker build -t sanrep/sphinx:6.2.1 .
