# Sphinx==6.2.1
sphinx-intl

# sphinx themes
sphinx_rtd_theme
sphinx_book_theme

# sphinx-simplepdf
weasyprint
git+https://github.com/useblocks/sphinx-simplepdf.git

# Linters
doc8
